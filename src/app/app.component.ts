import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {FakeProduct} from './select-product/select-product.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit{

  formProduct: FormGroup ;
  products: FakeProduct[] ;

  initialProductChoice: FakeProduct | null ;

  ngOnInit(): void {
    this.products = FakeProductData;
    this.initialProductChoice = this.products[3]; // initial choice value

    this.formProduct = new FormGroup(
      {product: new FormControl({...this.initialProductChoice})}
    );


    // -- only for watching of return object
    this.formProduct.valueChanges
      .subscribe( control => console.log('Your choice: ' , {...control.product}));
    // --

  }

}

const FakeProductData: FakeProduct[] = [
  { key: 'one' , name: 'Produkt 1'},
  { key: 'two' , name: 'Produkt 2'},
  { key: 'three', name: 'Produkt 3'},
  { key: 'four' , name: 'Produkt 4'},
  { key: 'five', name: 'Produkt 5'},
  { key: 'six', name: 'Produkt 6'},
];
