import {Component, forwardRef, Input} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
  selector: 'app-select-product',
  templateUrl: './select-product.component.html',
  styleUrls: ['./select-product.component.sass'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectProductComponent),
      multi: true }
  ]
})
export class SelectProductComponent implements ControlValueAccessor {


  @Input() itemList: FakeProduct[];

  value: FakeProduct;

  changeFn: any = () => {};

  constructor() {}

  writeValue(item: FakeProduct) {
    this.value = item;
    this.changeFn(this.value);
  }

  registerOnChange(fn): void {
    this.changeFn = fn;
  }

  registerOnTouched(fn: any) {
  }

  setDisabledState(isDisabled: boolean): void {}




  setItem(item: FakeProduct){
      // place for additional component actions

      this.writeValue(item);
  }

}


export interface FakeProduct {
  key: string;
  name: string;
}


